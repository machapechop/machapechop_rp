============================== Commands ==============================

That Command Works
---- Villager Treder ----
summon minecraft:villager ~ ~.5 ~ {VillagerData:{profession:librarian,level:99,type:jungle},Offers:{Recipes:[{buy:{id:command_block,Count:1,tag:{CustomModelData:1,display:{Name:'{"text":"Gold Coin","color":"yellow","bold":True}'}}},sell:{id:command_block,Count:10,tag:{CustomModelData:2,display:{Name:'{"text":"Silver Coin","color":"gray","bold":True}'}}},maxUses:999999,rewardExp:0b},{buy:{id:command_block,Count:10,tag:{CustomModelData:2,display:{Name:'{"text":"Silver Coin","color":"gray","bold":True}'}}},sell:{id:command_block,Count:1,tag:{CustomModelData:1,display:{Name:'{"text":"Gold Coin","color":"yellow","bold":True}'}}},maxUses:999999,rewardExp:0b},{buy:{id:command_block,Count:1,tag:{CustomModelData:2,display:{Name:'{"text":"Silver Coin","color":"gray","bold":True}'}}},sell:{id:command_block,Count:10,tag:{CustomModelData:3,display:{Name:'{"text":"Copper Coin","color":"gold","bold":True}'}}},maxUses:999999,rewardExp:0b},{buy:{id:command_block,Count:10,tag:{CustomModelData:3,display:{Name:'{"text":"Copper Coin","color":"gold","bold":True}'}}},sell:{id:command_block,Count:1,tag:{CustomModelData:2,display:{Name:'{"text":"Silver Coin","color":"gray","bold":True}'}}},maxUses:999999,rewardExp:0b}]},NoAI:1,Rotation:[180f,0f],Silent:1,Invulnerable:1,CustomName:"\"Cambio de Monedas\""}

/summon minecraft:villager ~ ~.5 ~ {VillagerData:{profession:mason,level:99},Offers:{Recipes:[{buy:{id:stone,Count:1},sell:{id:emerald,Count:1},maxUses:999999,rewardExp:100},{buy:{id:cobblestone,Count:1},sell:{id:emerald,Count:1},maxUses:999999,rewardExp:100},{buy:{id:netherrack,Count:1},sell:{id:emerald,Count:1},maxUses:999999,rewardExp:100}]}}

---- Custom Models ----

Thtopy
/give @p minecraft:paper{CustomModelData:1,display:{Name:'{"text":"Trofeo","color":"#00ff00","bold":True}'}}

Cuantum Pick
/give @p minecraft:netherite_pickaxe{CustomModelData:1,display:{Name:'{"text":"Quantumm Pick","color":"#00ff00","bold":True}'}}

StormLader
/give @p minecraft:netherite_axe{CustomModelData:1,display:{Name:'{"text":"StormLader","color":"#00ff00","bold":True}'}}

Gold Coin
/give @p minecraft:command_block{CustomModelData:1,display:{Name:'{"text":"Gold Coin","color":"yellow","bold":True}'}}

Silver Coin
/give @p minecraft:command_block{CustomModelData:2,display:{Name:'{"text":"Silver Coin","color":"gray","bold":True}'}}

Copper Coin
/give @p minecraft:command_block{CustomModelData:3,display:{Name:'{"text":"Copper Coin","color":"gold","bold":True}'}}

etharite
/give @p minecraft:command_block{CustomModelData:4,display:{Name:'{"text":"Etharite","color":"#00ff00","bold":True}'}}

etharite Pickaxe
/give @p minecraft:netherite_pickaxe{CustomModelData:2,display:{Name:'{"text":"Etharite Pickaxe","color":"#00ff00","bold":True}'}}

etharite Axe
/give @p minecraft:netherite_axe{CustomModelData:2,display:{Name:'{"text":"Etharite Axe","color":"#00ff00","bold":True}'}}

etharite Sword
/give @p minecraft:netherite_sword{CustomModelData:1,display:{Name:'{"text":"Etharite Sword","color":"#00ff00","bold":True}'}}

etharite Hoe
/give @p minecraft:netherite_hoe{CustomModelData:1,display:{Name:'{"text":"Etharite Hoe","color":"#00ff00","bold":True}'}}

shovel
/give @p minecraft:netherite_shovel{CustomModelData:1,display:{Name:'{"text":"Etharite Shovel","color":"#00ff00","bold":True}'}}

entity
/summon minecraft:zombie ~ ~.5 ~ {NoAI:1}


display names
/tellraw @a ["",{"text": "\uDB80\uDC00\uDB80\uDC02\uDB80\uDC09 Spectrasonic"}]
/tellraw @a ["",{"text": "0 \uDB80\uDC05 1 \uDB80\uDC06 2 \uDB80\uDC07 3 \uDB80\uDC08"}]

--- Lights ---

/give @p minecraft:light{BlockStateTag:{level:"0"}}
//used levels from 0 to 15
